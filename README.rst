DKIST User Documentation
========================

Welcome to the user documentation site for the Daniel K. Inouye Solar Telescope (DKIST).

This is the documentation site for the library hosting all of the data recorded by the DKIST during operations.
Principal Investigators (PIs) and Co-Investigators (Co-PIs) may search for their data on the DKIST Data Portal.
In addition, general data searches may be performed using the portal and the data downloaded for scientific
analysis.

First Steps
-----------
Not sure where to start? This is the place:

`DKIST Data Portal <https://dkist.data.nso.edu>`_

`How to search for Data <https://nso.atlassian.net/wiki/spaces/PDOI/pages/1802240116/DKIST+Data+Center+Search+Portal+Manual>`_

`Data Center Help Desk <https://nso.atlassian.net/servicedesk/customer/portals>`_

`Data Center User Tools <https://docs.dkist.nso.edu/projects/python-tools/en/latest>`_

:ref:`Data Center Calibration Processing <calibration>`

`DKIST Proposal Submission <https://nso.atlassian.net/servicedesk/customer/portal/3/article/247759667>`_

`DKIST Operations Help Desk <https://nso.atlassian.net/servicedesk/customer/portal/3/article/106103069>`_

Calibration Pipeline Documentation
----------------------------------
Each instrument has its own library of calibration codes:

| `VBI calibration documentation <https://docs.dkist.nso.edu/projects/vbi>`_
| `ViSP calibration documentation <https://docs.dkist.nso.edu/projects/visp>`_
| `CRYO-NIRSP calibration documentation <https://docs.dkist.nso.edu/projects/cryo-nirsp>`_


How the Documentation is Organized
----------------------------------
This site has lot of different kinds of documentation. A high-level overview of how it’s organized will help you know where to look for certain things:

:ref:`Tutorials <tutorials>` take you by the hand through a series of steps to search and download DKIST data. Start here if you’re new to the Data Center. Also look at the “First steps”.

:ref:`Topic guides <topic_guides>` discuss key topics and concepts at a fairly high level and provide useful background information and explanation.

:ref:`Reference guides <reference_guides>` contain technical reference for APIs and other aspects of the Data Center. They describe how things works and how to use them but assume that you have a basic understanding of key concepts.

:ref:`How-to guides <how_to_guides>` are recipes. They guide you through the steps involved in addressing specific use-cases. They are more advanced than tutorials and assume some knowledge of how the Data Center works.


Getting Help
------------
Need more help?
You can file a DKIST Help Desk ticket with your question `here. <https://nso.atlassian.net/servicedesk/customer/portals>`_

`How do I set up a DKIST Help Desk Account? <https://nso.atlassian.net/wiki/spaces/DHDT/pages/1738179064/DEV+How+to+set+up+a+DKIST+Help+Desk+Account>`_
Responses typically take 2-3 working days.

You can browse the
`NSO DKIST Web pages <https://www.nso.edu/telescopes/dkist>`_
or
`NSO Website <https://www.nso.edu/>`_
