.. _reference_guides:

Reference Guides
================

Reference guides contain technical reference for APIs and other aspects of the DKIST.
They describe how things works and how to use them but assume that you have a basic understanding of key concepts.

Calibration Processing APIs
---------------------------

Core
----
| The Core library provides a abstract interface to an underlying workflow library:
| `dkist-processing-core <https://docs.dkist.nso.edu/projects/core>`_

Common
------
| The Common library provides a set of common utility tasks for the calibration pipelines
| `dkist-processing-common <https://docs.dkist.nso.edu/projects/common>`_

Math
----
| The Math library provides a set of common mathematics functions for the calibration pipelines
| `dkist-processing-math <https://docs.dkist.nso.edu/projects/math>`_

Polarization Analysis and Calibration
-------------------------------------
| The PAC library provides a set of polarization analysis tools used by the calibration pipelines
| `dkist-processing-pac <https://docs.dkist.nso.edu/projects/pac>`_

Instrument Calibration Codes
----------------------------
| The Instrument libraries provide the set of calibration tools to calibrate the instrument data:
| `dkist-processing-vbi <https://docs.dkist.nso.edu/projects/vbi>`_
| `dkist-processing-visp <https://docs.dkist.nso.edu/projects/visp>`_
| `dkist-processing-cryonirsp <https://docs.dkist.nso.edu/projects/cryo-nirsp>`_
