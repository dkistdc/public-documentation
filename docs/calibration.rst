.. _calibration:

Calibration Processing
======================

Welcome to the DKIST Calibration Processing documentation pages.

The DKIST Data Center performs calibration processing on all DKIST observing data prior to its release to the scientific
community. The purpose of the calibrations is to remove any instrument- or telescope-related artifacts in the data and
prepare it for scientific analysis.

Once observing data has been received by the Data Center, calibration proceeds using a number of automated steps:

| Data Ingest
| Input Data Transfer
| Specific Calibration Steps:
|   Dark Correction
|   Gain Correction
|   Polarization Calibration
|   Geometric Calibration
|   etc.
| Movie Creation
|   Browse Movie Frame Creation
|   Browse Movie Assembly
| Output Data Transfer
| Quality Assessment
| Catalog and Quality Message Publication
| Teardown and Cleanup

Each of these steps is accomplished using a data-flow process described using a Directed Acyclic Graph. or DAG.

Directed Acyclic Graphs (DAGS)
------------------------------
An example of an end-to-end calibration DAG is shown in the image below.

| TBD image goes here

Instrument Calibration DAGS
---------------------------
Sample DAGs for each instrument are shown below.

| TBD images go here

Calibration Codes
-----------------
The calibration codes are implemented in several different libraries, each providing a different level of abstraction
and functionality. The libraries are:

| `Core <https://docs.dkist.nso.edu/projects/core>`_ - the abstraction layer to the underlying workflow library
| `Common <https://docs.dkist.nso.edu/projects/common>`_ - a set of common utility tasks used by the DAGs
| `Math <https://docs.dkist.nso.edu/projects/math>`_ - a set of common mathematical functions designed to operate efficiently over batches of array data
| `Polarization, Analysis and Calibration <https://docs.dkist.nso.edu/projects/pac>`_ - a set of common polarization tools used by the calibration pipelines

Instrument calibration codes - each instrument has its own library of calibration codes:

| `VBI <https://docs.dkist.nso.edu/projects/vbi>`_
| `ViSP <https://docs.dkist.nso.edu/projects/visp>`_
| `CRYO-NIRSP <https://docs.dkist.nso.edu/projects/cryo-nirsp>`_
